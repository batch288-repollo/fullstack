import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function CourseCard(props) {

	const {id, name, description, price} = props.courseProp;
	const {user} = useContext(UserContext);

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);

	function enroll() {
		if (count < 30) {
			setCount(count + 1);
			setSeat(seat - 1);
		}
		else {
			alert("No more seats!");
		}
	}

	useEffect(() => {
		if (seat === 0) {
			setIsDisabled(true);
			alert("No more seats after this!");
		}
	}, [seat]);

	return(
		<Container>
			<Row className="my-3">
				<Col className="col-12">
					<Card>
					    <Card.Body>
					    	<Card.Title className="mb-4">{name}</Card.Title>
					    	<Card.Subtitle className="mb-2">Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
					        <Card.Text>{price}</Card.Text>
					        <Card.Subtitle className="mb-2">Enrollees:</Card.Subtitle>
					        <Card.Text>{count} Enrollees</Card.Text>
					        {
					        	user !== null ?
					        	<Button className="mt-1" onClick={enroll} disabled={isDisabled}>Enroll</Button>
					        	:
					        	<Button as={Link} to='/login'>Login to Enroll</Button>
					        }
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
