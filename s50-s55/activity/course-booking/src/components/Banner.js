import {Button, Container, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner() {

	return (
		<Container>
			<Row>
				<Col className="mt-3 text-center">
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere!</p>
					<Button as={Link} to='/courses'>Enroll Now!</Button>
				</Col>
			</Row>
		</Container>
	);
}
