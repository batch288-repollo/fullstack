import {Navigate} from 'react-router-dom';
import {useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';


export default function Logout() {

	const {setUser, unsetUser} = useContext(UserContext);

	useEffect(() => {
		unsetUser();
		setUser(localStorage.getItem('email'));
	});

	return (
		<Navigate to='/login'/>
	);
}
