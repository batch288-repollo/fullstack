import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
		if (user !== null) {
			navigate('/notAccessible');
		}

		if (email !== '' && password !=='') {
			setIsDisabled(false);
		}
		else {
			setIsDisabled(true);
		}
	}, [email, password]);

	function login(event) {
		event.preventDefault();
		alert(`Welcome ${email}! You are now logged in.`);

		localStorage.setItem('email', email);
		setUser(localStorage.getItem('email'));

		setEmail('');
		setPassword('');
	}

	return(
		(user === null || user === '') ?
		<Container>
			<Row className="my-5">
				<Col className="col-6 mx-auto">
					<h1 className="mb-5 text-center">Login</h1>
					<Form onSubmit={event => login(event)}>
						<Form.Group className="mb-3" controlId="formBasicEmail">
					    	<Form.Label>Email address</Form.Label>
					        <Form.Control type="email" value={email} 
					        	onChange={event => setEmail(event.target.value)} 
					        	placeholder="Enter email" />
						</Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword1">
					    	<Form.Label>Password</Form.Label>
					        <Form.Control type="password" value={password} 
					        	onChange={event => setPassword(event.target.value)}
					        	placeholder="Password" />
					    </Form.Group>

					    <p className="mt-2">No account yet? <Link to='/register'>Sign up here.</Link></p>

					    <Button variant="primary" type="submit" disabled={isDisabled} className="mt-4 bg-success">
					    	Submit
					    </Button>
					</Form>
				</Col>
			</Row>
		</Container>
		:
		<Navigate to='/'/>
	);
}
