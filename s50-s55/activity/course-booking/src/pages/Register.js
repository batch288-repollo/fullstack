import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function Register() {

	const [isDisabled, setIsDisabled] = useState(true);
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [email, setEmail] = useState('');

	const {user, setUser} = useContext(UserContext);

	const navigate = useNavigate();


	useEffect(() => {
		if (user !== null) {
			navigate('/notAccessible');
		}

		if (email === '' || password1 === '' || password2 === '' 
			|| password1.length < 7 || (password1 != password2)) {
			setIsDisabled(true);
		}
		else {
			setIsDisabled(false);
		}
	}, [email, password1, password2]);

	function register(event) {
		event.preventDefault();
		alert(`Thank you for registering ${email}!`);

		setEmail('');
		setPassword1('');
		setPassword2('');

		localStorage.setItem('email', email);
		setUser(localStorage.getItem('email'));
		navigate('/');
	}

	return(
		<Container>
			<Row className="my-5">
				<Col className="col-6 mx-auto">
					<h1 className="mb-5 text-center">Register</h1>
					<Form onSubmit={event => register(event)}>
						<Form.Group className="mb-3" controlId="formBasicEmail">
					    	<Form.Label>Email address</Form.Label>
					        <Form.Control type="email" value={email} 
					        	onChange={event => setEmail(event.target.value)} 
					        	placeholder="Enter email" />
						</Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword1">
					    	<Form.Label>Password</Form.Label>
					        <Form.Control type="password" value={password1} 
					        	onChange={event => setPassword1(event.target.value)}
					        	placeholder="Password" />
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword2">
					    	<Form.Label>Confirm Password</Form.Label>
					        <Form.Control type="password" value={password2} 
					        	onChange={event => setPassword2(event.target.value)}
					        	placeholder="Re-type your nominated password." />
					    </Form.Group>

					    <p>Have an account already? <Link to='/login'>Login here.</Link></p>

					    <Button variant="primary" type="submit" disabled={isDisabled} className="mt-4">
					    	Submit
					    </Button>
					</Form>
				</Col>
			</Row>
		</Container>
	);
}
