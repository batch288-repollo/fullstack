import coursesData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js';

export default function Courses() {

	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
		);
	});

	return(
		<>
			<h1 className="text-center mt-3">Courses</h1>
			{courses}
		</>
	);
}
