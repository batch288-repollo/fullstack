import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';

import {UserProvider} from './UserContext.js';

import {BrowserRouter, Route, Routes, Redirect} from 'react-router-dom';
import {useState} from 'react';


function App() {

  const [user, setUser] = useState(localStorage.getItem('email'));

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route path='/'         element={<Home/>}     />
          <Route path='/courses'  element={<Courses/>}  />
          <Route path='/register' element={<Register/>} />
          <Route path='/login'    element={<Login/>}    />
          <Route path='/logout'   element={<Logout/>}   />
          <Route path='*'         element={<NotFound/>} />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
