// If you will create a ReactJS application. Type:
// npx create-react-app project-name
// npm start

// React JS is strict with the application name. It should be all in lower case.
// "courseBooking", "course Booking" - error or we were not able to create an application/project.

// Files to be deleted:
/*
	1. App.test.js
	2. index.css
	3. logo.svg
	4. reportWebvitals.js
	5. CodePart Only: package.json {"eslintConfig json"}
*/

// npm install react-bootstrap
// npm install bootstrap
// Set Port:
// "start": "set PORT=3001 && react-scripts start",
// npm install react-router-dom (installing ROUTES)
// npm install sweetalert2 (install SweetAlert2)
// npm install bcryptjs (install bcrypt)
// npm install jsonwebtoken (install jsonwebtoken)

// courses.js will serve as our mock database in place of our mongoDB because we want to focus on REACT JS concept.

// Environment variables are important for hiding sensitive information like the backend API URL which can be exploited
// if added directly into our code.
