import {Container, Row, Col, Card, Button} from 'react-bootstrap';
// Import the useState hook from react.
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext.js';


export default function CourseCard(props) {
	// console.log(props);
	// console.log(props.courseProp);

	const {_id, name, description, price, enrollees} = props.courseProp;
	const {user} = useContext(UserContext);

	// Use the state hook for this component to be able to store its state.
	// States are used to keep tract of information related to individual components.
	// Syntax: const [getter, setter] = useState(initialGetterValue);
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);

	function enroll() {
		if (count < 30) {
			setCount(count + 1);
			setSeat(seat - 1);
			// console.log(seat);
			// console.log(count);
		}
		else {
			alert("No more seats!");
		}
	}

	// The function or the side effect in our useEffect hook will be invoked or run on the initial loading 
	// of our application and when there is/change or changes on our dependencies.
	// [] - No dependencies meaning it will only trigger on first run / initial render.
	useEffect(() => {
		if (seat === 0) {
			setIsDisabled(true);
			alert("No more seats after this!");
		}
		// setIsDisabled(seat === 0);
	}, [seat]);

	useEffect(() => {
		setCount(enrollees.length);
		setSeat(seat - count);
	}, [])

	return(
		<Container>
			<Row className="my-3">
				<Col className="col-12">
					<Card>
					    <Card.Body>
					    	<Card.Title className="mb-4">{name}</Card.Title>
					    	<Card.Subtitle className="mb-2">Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
					        <Card.Text>{price}</Card.Text>
					        <Card.Subtitle className="mb-2">Enrollees:</Card.Subtitle>
					        <Card.Text>{count} Enrollees</Card.Text>
					        {
					        	user.id !== null ?
					        	<Button className="mt-1" as={Link} to={`/courses/${_id}`}>Details</Button>
					        	:
					        	<Button as={Link} to='/login'>Login to Enroll</Button>
					        }
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
