import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

// We use Link and NavLink to add hyperlink to our application.
// We use NavLink for navigation bars.
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';

import UserContext from '../UserContext.js';

// The 'as' keyword allows components to be treated as if they are a different component gaining access
// to its properties and functionalities.
export default function AppNavBar() {

	const {user} = useContext(UserContext);

	return(
		<>
			<Navbar bg="danger" variant="dark">
		    	<Container>
		        	<Navbar.Brand as={Link} to='/'>Zuitt</Navbar.Brand>
		        	<Nav className="ms-auto">
		        		<Nav.Link as={NavLink} to='/'>Home</Nav.Link>
		        		<Nav.Link as={NavLink} to='/courses'>Courses</Nav.Link>
		        		{
		        			user.id === null ? 
		        			<>
		        				<Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
		        				<Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
		        			</>
		        			:
		        			<Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
		        		}
		        	</Nav>
		        </Container>
		    </Navbar>
		</>
	);
}
