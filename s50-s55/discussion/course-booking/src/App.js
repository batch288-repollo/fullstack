import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import CourseView from './pages/CourseView.js';

// Import UserProvider:
import {UserProvider} from './UserContext.js';
import {decryptData} from './crypt.js';
import {useEffect} from 'react';

// The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content 
// and the shown URL in the web browser.
// The 'Routes' component holds all our 'Route' components.
// It selects which 'Route' component to show based on the url endpoint.
import {BrowserRouter, Route, Routes, Redirect} from 'react-router-dom';
import {useState} from 'react';

function App() {

  const token = localStorage.getItem('token');
  const tokenUsr = localStorage.getItem('tokenUsr');
  const tokenAdm = localStorage.getItem('tokenAdm');

  const [user, setUser] = useState({
    id: tokenUsr === null ? null : decryptData(tokenUsr),
    isAdmin: tokenAdm === null ? null : decryptData(tokenAdm)
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  // let userDetails = {}
  // useEffect(() => {
  //   if (token)
  //   {
  //     fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
  //       method: 'GET',
  //       headers: {
  //         Authorization: `Bearer ${token}`
  //       }
  //     }).then(result => result.json())
  //     .then(data => {
  //       userDetails = {
  //         id: data._id,
  //         isAdmin: data.isAdmin
  //       }
  //     });
  //   }
  //   else {
  //     userDetails = {
  //       id: null,
  //       isAdmin: null
  //     }
  //   }
  // }, []);

  useEffect(() => {
    console.log(`tokenUser: ${tokenUsr}`);
    console.log(`tokenAdmin: ${tokenAdm}`);
    console.log(user);
  }, [user]);

  return (
    // <div className="App">
    //   <h1>Hi from app.js</h1>
    // </div>
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route path='/'                   element={<Home/>}       />
          <Route path='/courses'            element={<Courses/>}    />
          <Route path='/register'           element={<Register/>}   />
          <Route path='/login'              element={<Login/>}      />
          <Route path='/logout'             element={<Logout/>}     />
          <Route path='/courses/:courseId'  element={<CourseView/>} />
          <Route path='*'                   element={<NotFound/>}   />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
