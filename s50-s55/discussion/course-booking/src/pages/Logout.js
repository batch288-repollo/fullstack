import {Navigate} from 'react-router-dom';
import {useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

export default function Logout() {

	const {setUser, unsetUser} = useContext(UserContext);

	// To clear the content of our local storage we use the function clear().
	useEffect(() => {
		unsetUser();
		setUser({
			id: null,
			isAdmin: null
		});
	});

	return (
		<Navigate to='/login'/>
	);
}
