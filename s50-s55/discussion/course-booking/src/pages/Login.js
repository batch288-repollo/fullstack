import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';
import {encryptData} from '../crypt.js';

import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';


export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	// The getItem() method gets the value of the specified key from our local storage.
	// const [user, setUser] = useState(localStorage.getItem('email'));

	// We are going to consume or use the UserContext.
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
		if (user.id !== null) {
			navigate('/notAccessible');
		}

		if (email !== '' && password !=='') {
			setIsDisabled(false);
		}
		else {
			setIsDisabled(true);
		}
	}, [email, password]);

	function login(event) {
		event.preventDefault();
		// alert(`Welcome ${email}! You are now logged in.`);
		// Set the email of the authenticated user in the local storage.
		/*
			Syntax:
			localStorage.setItem('propertyName', value);
		*/
		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'));
		// setEmail('');
		// setPassword('');

		// Process a fetch request to the corresponding backend API.
		// Syntax: fetch(url, {options});
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(result => result.json())
		.then(data => {
			if (data === false) {
				// alert('There was an error during authentication. Please try again or signup first!');
				Swal2.fire({
					title: 'Authentication failed!',
					icon: 'error',
					text: 'Check your login details and try again.'
				});
			}
			else {
				// alert("Log in successful!");
				Swal2.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				});
				localStorage.setItem('token', data.auth);
				getUserDetails(data.auth);
				// navigate('/');
			}
		});
	}

	function getUserDetails(token) {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			localStorage.setItem('tokenAdm', encryptData(data.isAdmin.toString()));
			localStorage.setItem('tokenUsr', encryptData(data._id));
		});
	}

	return(
		user.id === null ?
		<Container>
			<Row className="my-5">
				<Col className="col-6 mx-auto">
					<h1 className="mb-5 text-center">Login</h1>
					<Form onSubmit={event => login(event)}>
						<Form.Group className="mb-3" controlId="formBasicEmail">
					    	<Form.Label>Email address</Form.Label>
					        <Form.Control type="email" value={email} 
					        	onChange={event => setEmail(event.target.value)} 
					        	placeholder="Enter email" />
						</Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword1">
					    	<Form.Label>Password</Form.Label>
					        <Form.Control type="password" value={password} 
					        	onChange={event => setPassword(event.target.value)}
					        	placeholder="Password" />
					    </Form.Group>

					    <p className="mt-2">No account yet? <Link to='/register'>Sign up here.</Link></p>

					    <Button variant="primary" type="submit" disabled={isDisabled} className="mt-4 bg-success">
					    	Submit
					    </Button>
					</Form>
				</Col>
			</Row>
		</Container>
		:
		<Navigate to='/'/>
	);
}
