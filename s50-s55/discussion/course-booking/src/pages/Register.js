import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {encryptData} from '../crypt.js';

import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';


export default function Register() {

	const [isDisabled, setIsDisabled] = useState(true);
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [number, setNumber] = useState('');
	const [passwordMsg, setPasswordMsg] = useState('');

	const {user, setUser} = useContext(UserContext);

	// We contain the useNavigate() method to variable navigate.
	const navigate = useNavigate();

	useEffect(() => {
		if (user.id !== null) {
			navigate('/notAccessible');
		}

		if (password1 != password2 && password2.length > 0) {
			setPasswordMsg('(Passwords not match!)')
		}
		else {
			setPasswordMsg('')
		}

		if (email === '' || password1 === '' || password2 === '' 
			|| password1.length < 7 || (password1 != password2)
			|| firstName === '' || lastName === '' || number === '' 
			|| number.length != 11) {
			setIsDisabled(true);
		}
		else {
			setIsDisabled(false);
		}
	}, [email, password1, password2, firstName, lastName, number]);

	async function register(event) {
		event.preventDefault();

		await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: number
			})
		}).then(result => result.json())
		.then(data => {
			if (data === false) {
				Swal2.fire({
					title: 'Register Failed',
					icon: 'error',
					text: 'Email already registered. Try logging in.'
				});
			}
			else {
				Swal2.fire({
					title: 'Register Successful',
					icon: 'success',
					text: `Welcome to Zuitt ${firstName}!`
				});

				login();
				navigate('/');
			}
		});

		// setEmail('');
		// setPassword1('');
		// setPassword2('');
		// setFirstName('');
		// setLastName('');
		// setNumber('');
	}

	function login() {
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		}).then(result => result.json())
		.then(data => {
			if (data) {
				localStorage.setItem('token', data.auth);
				getUserDetails(data.auth);
			} else {
				console.log('Cannot Login!');
			}
		});
	}

	function getUserDetails(token) {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			localStorage.setItem('tokenAdm', encryptData(data.isAdmin.toString()));
			localStorage.setItem('tokenUsr', encryptData(data._id));
		});
	}

	return(
		<Container>
			<Row className="my-5">
				<Col className="col-6 mx-auto">
					<h1 className="mb-5 text-center">Register</h1>
					<Form onSubmit={event => register(event)}>
						<Form.Group className="mb-3" controlId="formBasicEmail">
					    	<Form.Label>Email address</Form.Label>
					        <Form.Control type="email" value={email} 
					        	onChange={event => setEmail(event.target.value)} 
					        	placeholder="Enter email" />
						</Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword1">
					    	<Form.Label>Password</Form.Label>
					        <Form.Control type="password" value={password1} 
					        	onChange={event => setPassword1(event.target.value)}
					        	placeholder="Password" />
					    </Form.Group>

					    <Form.Group className="mb-5" controlId="formBasicPassword2">
					    	<Form.Label>Confirm Password <a style={{color: 'red'}}>{passwordMsg}</a></Form.Label>
					        <Form.Control type="password" value={password2} 
					        	onChange={event => setPassword2(event.target.value)}
					        	placeholder="Re-type your nominated password." />
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicFirstName">
					    	<Form.Label>First Name</Form.Label>
					        <Form.Control type="text" value={firstName} 
					        	onChange={event => setFirstName(event.target.value)}
					        	placeholder="Enter your first name." />
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicLastName">
					    	<Form.Label>Last Name</Form.Label>
					        <Form.Control type="text" value={lastName} 
					        	onChange={event => setLastName(event.target.value)}
					        	placeholder="Enter your last name." />
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicNumber">
					    	<Form.Label>Mobile Number</Form.Label>
					        <Form.Control type="number" value={number} 
					        	onChange={event => setNumber(event.target.value)}
					        	placeholder="Enter your mobile number." />
					    </Form.Group>

					    <p className="mt-5">Have an account already? <Link to='/login'>Login here.</Link></p>

					    <Button variant="primary" type="submit" disabled={isDisabled} className="mt-4">
					    	Submit
					    </Button>
					</Form>
				</Col>
			</Row>
		</Container>
	);
}
