import {Link} from 'react-router-dom';
import {Container, Row, Col} from 'react-bootstrap';

export default function NotFound() {
	return (
		<>
			<Container>
				<Row>
					<Col className="col-6 mx-auto text-center mt-5">
						<h1>404 - Page Not Found</h1>
						<h4 className="my-4">Go back to the <Link to='/'>hompage</Link>.</h4>
						<img className="img-fluid border border-danger border-5 mt-5"
						src="https://www.pngitem.com/pimgs/m/255-2550411_404-error-images-free-png-transparent-png.png"/>
					</Col>
				</Row>
			</Container>
		</>
	);
}
