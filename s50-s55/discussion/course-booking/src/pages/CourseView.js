import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';

import Swal2 from 'sweetalert2';


export default function CourseView() {
	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');

	const navigate = useNavigate();

	const {courseId} = useParams();
	// console.log(courseId);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setPrice(data.price);
			setDescription(data.description);
		});
	}, []);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				courseId: courseId
			})
		}).then(response => response.json())
		.then(data => {
			// console.log(data);
			if (data) {
				Swal2.fire({
					title: 'Successfullt enrolled!',
					icon: 'success',
					text: 'You have successfully enrolled for this course.'
				});
				navigate('/courses');
			}
			else {
				Swal2.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again.'
				});
			}
		});
	}

	return(
		<Container>
			<Row>
				<Col className="col-12 my-3">
					<Card>
						<Card.Body>
							<Card.Title className="mb-3">{name}</Card.Title>
					    	<Card.Subtitle>Description:</Card.Subtitle>
					    	<Card.Text>{description}</Card.Text>
					    	<Card.Subtitle>Price:</Card.Subtitle>
					    	<Card.Text>{price}</Card.Text>
					    	<Card.Subtitle>Class Schedule:</Card.Subtitle>
					    	<Card.Text>8am - 5pm</Card.Text>
					    	<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
