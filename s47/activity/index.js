// HTML Elements:
let firstName = document.querySelector('#txt-first-name');
let lastName = document.querySelector('#txt-last-name');
let fullName = document.querySelector('#fullName');
let textColor = document.querySelector('#text-color');

// Event Listeners:
firstName.addEventListener('keyup', () => {
	console.log(firstName.value);
	fullName.innerHTML = `${firstName.value} ${lastName.value}`;
});

lastName.addEventListener('keyup', () => {
	console.log(lastName.value);
	fullName.innerHTML = `${firstName.value} ${lastName.value}`;
});

textColor.addEventListener('change', () => {
	fullName.style.color = textColor.value;
});

