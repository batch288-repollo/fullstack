// console.log('Hey!');

// [Section] Document Object Model (DOM)
// It allows us to access or modify the properties of an HTML element in a web page.
// It is standard on how to get, change, add or delete HTML elements.
// We will be focusing only with DOM in terms of managing forms.

// Using the query selector, it can access/get the HTML element/s.
// Use CSS selectors to target specific element.
/*
	Id selector (#),
	Class selector (.),
	Tag/type selector (html tags),
	Universal selector (*),
	Attribute selector ([attribute])
*/
// Query selector has two types: querySelector and querySelectorAll
let firstElement = document.querySelector('#txt-first-name');
console.log(firstElement);

// querySelector:
let secondElement = document.querySelector('.full-name');
console.log(secondElement);

// querySelectorAll:
let thirdElement = document.querySelectorAll('.full-name');
console.log(thirdElement);

// getElements
let element = document.getElementById('fullName');
console.log(element);

element = document.getElementsByClassName('full-name');
console.log(element);


// [Section] Event Listeneres:
// Whenever a user interacts with a webpage, this action is considered as an event.
// Working with events is a large part of creating interactivity in a webpage.
// Specific function will be invoked if the event happen.
// The function 'addEventListener()' takes two arguments.
/*
	Parameters:
	1. A string identifying the event.
	2. Function that the listener will invoke once the specified event occur.
*/
// 'value' property only works for input elements.

let fullName = document.querySelector('#fullName');
console.log(fullName.innerHTML);
// console.log(fullName.textContent);

firstElement.addEventListener('keydown', () => {
	console.log(firstElement.value);
	fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`;
});

// Mini-Activity:
let txtLastName = document.querySelector('#txt-last-name');
txtLastName.addEventListener('keyup', () => {
	console.log(txtLastName.value);
	fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`;
});

