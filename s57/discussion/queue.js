let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    if (collection.length <= 0) {
        return collection;
    }
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    // if (collection.length <= 0) {
    //     collection[0] = element;
    // }
    // else {
    //     collection[collection.length] = element;
    // }
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    // In here you are going to remove the last->first element in the array

    let tempCollection = []
    if (collection.length > 0) {
        for (let i = 1; i < collection.length; i++)
        {
            tempCollection[i-1] = collection[i];
        }
        collection = tempCollection;
    }
    return collection;
}

function front() {
    // In here, you are going to remove->get the first element
    return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    return collection.length;
}

function isEmpty() {
    //it will check whether the function is empty or not
    if (collection.length <= 0) {
        return true;
    }
    else {
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};