// console.log('Ayt!');

let postsEntries = document.querySelector('#div-post-entries');
let txtTitle = document.querySelector('#txt-title');
let txtBody = document.querySelector('#txt-body');

let txtEditTitle = document.querySelector('#txt-edit-title');
let txtEditBody = document.querySelector('#txt-edit-body');
let editId = document.querySelector('#txt-edit-id');
let editButton = document.querySelector('#btn-submit-update');

let entries = '';
let idNumber = 1;


// Fetch keyword:
// fetch('url' {options});
fetch('https://jsonplaceholder.typicode.com/posts')
.then(result => result.json())
.then(response => {
	// The response is an array of object.
	console.log(response);
	showPosts(response);
}).catch(error => console.log(error));

const showPosts = (posts) => {
	console.log(typeof posts);

	posts.forEach(post => {
		entries += `
		<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${idNumber}. ${post.title.toUpperCase()}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost(${post.id})" id = "btn-edit-${post.id}">Edit</button>
			<button onclick = "deletePost(${post.id})" id = "btn-delete-${post.id}">Delete</button>
		</div>
		`;

		idNumber++;
	});

	postsEntries.innerHTML = entries;
	console.log(postsEntries);
}

// Post data in our API.
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	// When the submit event is used, we must add parameter event to the function to capture the properties of our event.

	// To change the auto-reload of the submit method.
	event.preventDefault();

	// POST method:
	// If we use the post request the fetch method will return the newly created document.
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: txtTitle.value,
			body: txtBody.value,
			userId: idNumber,
		})
	}).then(response => response.json())
	.then(result => {
		console.log(result);
		alert('Post was successfully added!');
		addToPost();
	});
});

// Updating the post.
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
	event.preventDefault();

	let id = editId.value;

	fetch(`https://jsonplaceholder.typicode.com/put/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: txtEditTitle.value,
			body: txtEditBody.value,
			userId: id
		})
	}).then(response => response.json())
	.then(result => {
		console.log(result);
		alert('The post was successfully updated!');
		updatePost(id);
	});
});

const addToPost = () => {
	entries += `
	<div id = "post-${idNumber}">
		<h3 id = "post-title-${idNumber}">${idNumber}. ${txtTitle.value}</h3>
		<p id = "post-body-${idNumber}">${txtBody.value}</p>
		<button onclick = "editPost(${idNumber})" id = "btn-edit-${idNumber}">Edit</button>
		<button onclick = "deletePost(${idNumber})" id = "btn-delete-${idNumber}">Delete</button>
	</div>
	`;

	idNumber++;
	postsEntries.innerHTML = entries;

	txtTitle.value = null;
	txtBody.value = null;
}

const editPost = (id) => {
	console.log(id);
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	console.log(title);
	console.log(body);

	txtEditTitle.value = title;
	txtEditBody.value = body;
	editId.value = id;

	// removeAttribute() will remove the declared attribute from the element.
	editButton.removeAttribute('disabled');
}

const updatePost = (id) => {
	document.querySelector(`#post-title-${id}`).innerHTML = txtEditTitle.value;
	document.querySelector(`#post-body-${id}`).innerHTML = txtEditBody.value;

	// To disable button:
	// setAttribute() method adds attribute to the selected element.
	editButton.setAttribute('disabled', true);
	// editButton.disabled = true;

	txtEditTitle.value = null;
	txtEditBody.value = null;
}

const deletePost = (id) => {
	console.log(id);
	document.querySelector(`#post-title-${id}`).remove();
	document.querySelector(`#post-body-${id}`).remove();
	document.querySelector(`#btn-edit-${id}`).remove();
	document.querySelector(`#btn-delete-${id}`).remove();

	fetch(`https://jsonplaceholder.typicode.com/delete/${id}`, {method: 'DELETE'})
	.then(response => response.json())
	.then(result => {
		console.log(result);
		alert('Post was successfully deleted!');
	});
}
